"""These methods are for use in the flask shell."""
from tests.factories import CollectionFactory


class seeder(object):
    """Generates a post dialog history."""

    def __init__(self, db):
        """Initalize."""
        self.db = db

    def create_seed_data(self, num):
        """Create seed data."""
        self.collections = []
        for i in range(num):
            self.collections.append(CollectionFactory())
            if i < num - 1:
                self.collections[i].in_progress = False
                winner = max(self.collections[i].posts,
                             key=lambda p: p.votes)
                self.collections[i].selected = winner

    def commit_seed_data(self):
        """Commit seed data to database."""
        self.db.create_all()
        self.db.session.add_all(self.collections)
        self.db.session.commit()

    def drop_all(self):
        """Drop Database."""
        self.db.session.commit()
        self.db.drop_all()
