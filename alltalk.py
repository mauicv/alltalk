"""Alltalk flask app."""

from app import flask_app
from app import db
from app.models import Collection, SuggestedPost
from tests.factories import PostFactory, CollectionFactory
from shell_methods import seeder


@flask_app.shell_context_processor
def make_shell_context():
    """Generate context for use in shell."""
    return {
        'db': db,
        "col": Collection,
        "sugPo": SuggestedPost,
        "poFac": PostFactory,
        "colFac": CollectionFactory,
        "seed": seeder(db)
        }
