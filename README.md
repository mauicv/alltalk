## All talk development setup:

----

### Setup:

1. Run `vagrant up` to setup the development environment. This will give you the following:
  - A Linux virtual machine
  - `python3.6`
  - `pip 18.1`
  - `postgresql 10.0`
  - `git 2.7.4`
  - a python virtual environment called `venv`
  - an alias `alta` that will start up `venv` and put you in the project folder.
  - a user called "vagrant" in superuser role with password "vagrant" and a postgresql database called "vagrant"
2. Run `alta`
3. Run `pip install -r requirements.txt`

---
## Testing:
we currently only use `nose2` for testing.

---
