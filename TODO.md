## TODO

### Immidiate

- [x] create requirements.txt and add to README.md setup.
- [x] create `suggested_post` database model.
  Should have the following fields:
  - `post_contents` - `string`: suggested response to the previous selected `suggested_post`
  - `time` - `datetime`: time `suggested_post` submitted.
  - `collection_id` - `int`: should refer to the collection of `suggested_posts` considered as a response to the previous selected `suggested_post`.
  - `id` - `int`
  - `votes` - `int` : number of votes for that `suggest_post`
- [x] create a `timeline` database which has the following feilds:
  - `id` - `int`: The `collection_id`
  - `in_progress` - `boolean`: weather or not that collection has resolved
  - `selected_post` - `int`:  the outcome of a resolved collection.
- [x] create initial seeder for database
  - collection of `suggested_posts` with different `collection_id` values.
- [x] `destroy` and `save` methods.
- [x] Incorporate factories into seperate helper methods file.
- [ ] Build `front-end` blueprint route that serves html/javascript:
  - [ ] Index page
    1. voting on current suggested posts
    2. viewing selected posts
    3. requesting more selceted posts
    4. creating new suggested post
  - [ ] About page
    1. explination of project
  - [ ] History page
    1. Allow users to view unselected suggested posts and voting information, etc...(to be considered...)
- [ ] Build `backend` blueprint routes that take querys and respond with data.
  - [ ] Get `selected_posts` from `date1` - `date2` collections:
  - [ ] Post vote:
  - [ ] Get `Suggested_post`
  - [ ] Post `suggested_post`  
- [ ]
- [ ] Generate dithered time selections for factory posts each within allotted time bins.
- [ ] set up `apscheduler` to periodically run the updates
- [ ] set up nose2 for testing
- [ ] set up factories for dev, testing and production.
- display:
  - [ ] Get `n` most recent posts where `n` is the number of posts that will fill the screen.
- test data:
  - [ ] fix factory error: `in_progress=true` collection shouldn't have a selectedd post.

___

### Notes:

- Decide:
  - [x] consider flask ran wedserver...
    - [ ] Create template that displays all posts.
    - [ ] Create form for voting on suggested_posts
    - [ ] Create form for submitting a suggested_post.
  - [ ] or a javascipt frontend with a flask backend
    - [ ] could use vuejs or react native
    - [ ] Would leave the backend as a super simple api
    - [ ] consolidates knowledge
    - [ ] makes testing backend simpler
    - [ ] will probabaly be easier to scale.


- (15/10/18) start with everything flask. At some point when it comes to load baring and expansion we'll have to design an api access layer to the service and have either js or python server apps sitting on top.
- (18/10/18) Build an access api layer and serve simple html and javascript via the flask routes. Possiblly will upgrade this solution to a vue page at a later date. This need two blueprints.
  1. The first to handle querys and data transfer. (API layer)
  2. The second to serve the html/javascript that displays the page and comunicates with the server. (webapp)     

### Far Flung/ Unrelated

- [ ] Create project shell for quick implementation of flask backend. See...
