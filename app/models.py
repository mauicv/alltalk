"""Database models."""

from app import db
import datetime


class SuggestedPost(db.Model):
    """Defines structure of a suggested post."""

    __tablename__ = 'suggested_post'
    id = db.Column(db.Integer, primary_key=True)
    post_contents = db.Column(db.String(500), index=True)
    time_stamp = db.Column(db.DateTime,
                           nullable=False,
                           default=datetime.datetime.utcnow)
    collection_id = db.Column(db.Integer, db.ForeignKey('collection.id'))
    votes = db.Column(db.Integer)

    def __repr__(self):
        """Instance -> str."""
        return '<Post id: {}>'.format(self.id)


class Collection(db.Model):
    """Defines a Collection which is a group of suggested posts.

    When all votes are polled and the verdict is in the in_progress field
    becomes False and selected will refer to the SuggestedPost that won.
    """

    __tablename__ = 'collection'
    id = db.Column(db.Integer, primary_key=True)
    in_progress = db.Column(db.Boolean, nullable=False, default=True)
    selected = db.relationship('SuggestedPost', uselist=False)
    posts = db.relationship("SuggestedPost", backref="collection")

    def __repr__(self):
        """Instance -> str."""
        return '<Collection_id: {}>'.format(self.id)
