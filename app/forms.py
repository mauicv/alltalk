"""Input forms."""

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class Suggestion_form(FlaskForm):
    """Form to suggest a new post."""

    suggested_post = StringField('What would you say:',
                                 validators=[DataRequired()])
    submit_suggestion = SubmitField('Submit')


class More_form(FlaskForm):
    """Form to suggest a new post."""

    submit_more = SubmitField('Submit')
