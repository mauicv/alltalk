"""Routes.py."""
import random

from app import flask_app
from flask import render_template, jsonify
from app import db
from app.models import Collection
from app.forms import Suggestion_form, More_form
# from app.templates import suggest


# add a parameter for number of posts to load.
@flask_app.route('/', methods=['GET', 'POST'])
@flask_app.route('/index', methods=['GET', 'POST'])
def index():
    """Provide main page of application."""
    collections = db.session.query(Collection).all()
    db_posts = []
    for collection in collections:
        if not collection.in_progress:
            db_posts.append(collection.selected.post_contents)
        else:
            sug_post = [post.post_contents for post
                        in collection.posts]
    chosen_post = sug_post[random.randint(0, len(sug_post)-1)]

    more_form = More_form()
    suggestion_form = Suggestion_form()

    if more_form.validate_on_submit():
        print(more_form.data)

    return render_template('suggest.html',
                           title='index',
                           db_posts=db_posts,
                           chosen_post=chosen_post,
                           more_form=more_form,
                           suggestion_form=suggestion_form)
