"""Query routes.

Functionality:
1. Post new suggested_post
2. Vote on suggested_posts
3. Request more selected posts
"""

from flask import Blueprint, jsonify
# from app import db

from .forms import Suggestion_form, More_form

query_bp = Blueprint('query', __name__,
                  template_folder='templates')


@query_bp.route('/more', methods=['GET', 'POST'])
def more():
    more_from = More_form()
    if more_from.validate_on_submit():
        print("more")
    pass
    return jsonify(
             response="more"
         )


@query_bp.route('/suggest', methods=['GET', 'POST'])
def suggest():
    Suggest_form = Suggestion_form()
    if Suggest_form.validate_on_submit():
        print("suggest")
    pass
    return jsonify(
            response="suggestion"
        )


# @flask_app.route('/suggestion', methods=['GET', 'POST'])
# def suggestion():
#     """Deal with suggested posts."""
#     suggestion_form = Suggestion_form()
#     if suggestion_form.validate_on_submit():
#         print(suggestion_form.data)
#
#     return jsonify(
#             response="suggestion"
#         )
