from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy

flask_app = Flask(__name__)
flask_app.config.from_object(Config)
db = SQLAlchemy(flask_app)

from app.query_bp import query_bp
flask_app.register_blueprint(query_bp)

from app import routes
