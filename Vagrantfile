# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/xenial64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  #config.vm.network "forwarded_port", guest: 27017, host: 27017, host_ip: "127.0.0.1"
  config.vm.network "forwarded_port", guest: 5000, host: 5000,  host_ip: "127.0.0.1"


  #config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Required for NFS to work, pick any local IP
  config.vm.network :private_network, ip: '192.168.50.50'
  # Use NFS for shared folders for better performance
  config.vm.synced_folder '.', '/vagrant'

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #

  #   vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
  # Customize the amount of memory on the VM:
     vb.memory = 2048
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  #config.vm.provision "shell", inline: <<-SHELL
  #    'echo provisioning script:'
  #   apt-get update
  # apt-get install -y apache2

  config.vm.provision "shell", inline: $script, privileged: false

  # :yields: argumentsconfig.vm.provision "shell", path: "provision.sh", privileged: false
  #config.vm.provision "shell", path: "provision.sh"

  #config.vm.provision :shell, :path => "provision.sh"
  #SHELL
end
$script = <<-SCRIPT
echo I am provisioning...
date > /etc/vagrant_provisioned_at

sudo apt-get -y install build-essential libffi-dev python-dev python3-dev

# First we set up python3.6 and create a virtual environment venv:

sudo apt-get -y upgrade

sudo add-apt-repository -y ppa:jonathonf/python-3.6
sudo apt-get -y update
sudo apt-get -y install python3.6

cd ~
python3.6 -m venv venv --without-pip
cd venv/
source bin/activate
curl https://bootstrap.pypa.io/get-pip.py | python3

sudo apt-get -y install git
sudo apt-get -y install python3.6-dev

# add alta alias to ~/.bashrc
echo "alias alta='source ~/venv/bin/activate && cd ~/../../vagrant'" >> ~/.bashrc

# set up a postgresql server
sudo add-apt-repository 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y postgresql-10

# create user, pasword and table all called vagrant.
sudo -u postgres createuser --superuser vagrant
sudo -u postgres createdb vagrant
sudo -u vagrant psql -c "ALTER USER postgres WITH PASSWORD 'vagrant';"
sudo -u vagrant createdb alltalk-test

SCRIPT
