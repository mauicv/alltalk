"""Factory Methods for testing."""
import factory
import random
from faker import Faker

from app.models import SuggestedPost, Collection

fake = Faker()


class PostFactory(factory.Factory):
    """Create Post with random content."""

    class Meta: # noqa
        model = SuggestedPost

    id = factory.Sequence(lambda n: n)
    post_contents = factory.Sequence(lambda n: fake.text())
    votes = factory.Sequence(lambda _: random.randint(1, 250))


class CollectionFactory(factory.Factory):
    """Create collection with 4 posts."""

    class Meta: # noqa
        model = Collection

    id = factory.Sequence(lambda n: n)
    posts = factory.List([factory.SubFactory(PostFactory, collection_id=factory
                         .SelfAttribute("id"))
                         for i in range(4)])
