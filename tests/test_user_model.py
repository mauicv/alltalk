import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )

from app.models import User

def test_user_password_hash():
    u=User(username='testuser')
    u.set_password('test')
    assert u.check_password('test')==True
